package tasks.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import tasks.model.Task;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

public class WBTest {
    private Task task;

    @BeforeEach
    void setUp() {
        task = new Task("task01", new Date(2020,3,23), new Date(2020,3,25), 2);
    }

    @Test
    void testNullDate() {
        assertThrows(NullPointerException.class, ()-> {task.getDateForRepeatedTask(null);});
    }

    @Test
    void testDateBeforeStart() {
        try {
            assertEquals(task.getDateForRepeatedTask(new Date(2020, 3, 20)), task.getStartTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void testDateEqualsStart() {
        try {
            assertEquals(task.getDateForRepeatedTask(new Date(2020, 3, 23)), new Date(2020,3,25));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void testDateAfterEnd() {
        assertThrows(Exception.class, ()-> {task.getDateForRepeatedTask(new Date(2020, 3, 28));});
    }

    @Test
    void testDateBetweenStartAndEnd() {
        try {
            assertEquals(task.getDateForRepeatedTask(new Date(2020, 3, 24)), new Date(2020,3,24));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
