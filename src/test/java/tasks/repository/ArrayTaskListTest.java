package tasks.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import tasks.model.Task;
import tasks.repository.ArrayTaskList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

public class ArrayTaskListTest {
    private ArrayTaskList repo;
    private SimpleDateFormat sdf;
    private Calendar calendar = Calendar.getInstance();

    @BeforeEach
    void setUp() {
        repo = new ArrayTaskList();
        sdf = new SimpleDateFormat("dd/MM/yyyy");
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @Tag("ECP")
    @DisplayName("Valid test ECP")
    void TC1_ECP() {
        calendar.set(2020,04,06);
        Date startDate = calendar.getTime();
        Task task = new Task("Task1", startDate, new Date(2020, 4, 20), 15);
        repo.add(task);

        assertEquals(repo.size(), 1);
    }

    @Test
    @Tag("ECP")
    @DisplayName("Invalid test ECP for interval")
    void TC2_ECP() {
        boolean isError = false;
        try {
            calendar.set(2020, 04, 06);
            Date startDate = calendar.getTime();
            Task task = new Task("Task1", startDate, new Date(2020, 4, 20), -2);
            repo.add(task);
        }
        catch (IllegalArgumentException e) {
            isError = true;
        }
        finally {
            assertEquals(isError, true);
        }

        assertEquals(repo.size(), 0);
    }

    @Test
    @Tag("ECP")
    @DisplayName("Invalid test ECP for startDate")
    void TC3_ECP() {
        calendar.set(2020,-2,06);
        Date startDate = calendar.getTime();
        System.out.println(startDate.toString());
        Task task = new Task("Task1", startDate, new Date(2020, 4, 20), 15);
        repo.add(task);
        assertEquals(checkYear(startDate, 2020), false);
    }

    @Test
    @Tag("ECP")
    @DisplayName("Invalid test ECP for interval and startDate")
    void TC6_ECP() {
        boolean isError = false;
        Date startDate = new Date();
        try {
            calendar.set(2020, 14, 6);
            startDate = calendar.getTime();
            Task task = new Task("Task1", startDate, new Date(2020, 4, 20), -2);
            repo.add(task);
        }
        catch (IllegalArgumentException e) {
            isError = true;
        }
        finally {
            assertEquals(isError, true);
            assertEquals(checkYear(startDate, 2020), false);
        }
    }

    @Test
    @Tag("BVA")
    @DisplayName("Invalid BVA test for interval")
    void TC1_BVA() {
        boolean isError = false;
        try {
            calendar.set(2020, 04, 06);
            Date startDate = calendar.getTime();
            Task task = new Task("Task1", startDate, new Date(2020, 4, 20), -1);
            repo.add(task);
        }
        catch (IllegalArgumentException e) {
            isError = true;
        }
        finally {
            assertEquals(isError, true);
        }

        assertEquals(repo.size(), 0);
    }

    @Test
    @Tag("BVA")
    @DisplayName("Valid BVA test for interval")
    void TC5_BVA() {
        calendar.set(2020,04,06);
        Date startDate = calendar.getTime();
        Task task = new Task("Task1", startDate, new Date(2020, 4, 20), Integer.MAX_VALUE);
        repo.add(task);

        assertEquals(repo.size(), 1);
    }

    @ParameterizedTest
    @ValueSource(ints = {11,12})
    @Tag("BVA")
    @DisplayName("Valid BVA test for startDate")
    void TC10_BVA(int month) {
        calendar.set(2020,month,06);
        Date startDate = calendar.getTime();
        Task task = new Task("Task1", startDate, new Date(2020, 4, 20), 15);
        repo.add(task);

        assertEquals(repo.size(), 1);
    }

    @Test
    @Tag("BVA")
    @DisplayName("Valid BVA test for startDate")
    void TC12_BVA() {
        calendar.set(2020,13,06);
        Date startDate = calendar.getTime();
        System.out.println(startDate.toString());
        Task task = new Task("Task1", startDate, new Date(2020, 4, 20), 15);
        repo.add(task);
        assertEquals(checkYear(startDate, 2020), false);
    }

    @Test
    @Disabled
    void dummyTest() {

    }

    //check whether the year is the same as the year from date
    private boolean checkYear(Date date, int year) {
        String[] dateString = date.toString().split(" ");
        if(year != Integer.parseInt(dateString[dateString.length - 1]))
            return false;
        return true;
    }

}