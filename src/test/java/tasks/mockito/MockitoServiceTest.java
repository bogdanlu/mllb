package tasks.mockito;

import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import tasks.model.Task;
import tasks.repository.ArrayTaskList;
import tasks.services.TasksService;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MockitoServiceTest {
    @Spy
    private ArrayTaskList tasks;

    private TasksService tasksService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        tasks.add(new Task("titlu", new Date()));
        tasksService = new TasksService(tasks);
    }

    @Test
    public void test_get_observable_list() {
        ObservableList<Task> taskObservableList = tasksService.getObservableList();
        assertEquals(taskObservableList.size(), 1);

        Mockito.verify(tasks, Mockito.times(1)).getAll();
    }

    @Test
    public void test_add_task() {
        tasksService.add("titlu", new Date());
        assertEquals(tasks.size(), 2);
    }

}
