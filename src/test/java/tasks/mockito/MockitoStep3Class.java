package tasks.mockito;

import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import tasks.model.Task;
import tasks.repository.ArrayTaskList;
import tasks.services.TasksService;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MockitoStep3Class {

    private Task task;

    private ArrayTaskList taskList;

    private TasksService tasksService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        task = new Task("titlu", new Date());
        taskList = new ArrayTaskList();
        taskList.add(task);
        tasksService = new TasksService(taskList);
    }

    @Test
    public void test_valid_get_observable() {
        ObservableList<Task> taskObservableList = tasksService.getObservableList();
        assertEquals(taskObservableList.size(), 1);
    }

    @Test
    public void test_valid_add() {
        tasksService.add("titlu", new Date());
        assertEquals(tasksService.getNumberOfTasks(), 2);
    }
}
