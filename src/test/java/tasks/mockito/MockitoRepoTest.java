package tasks.mockito;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import tasks.model.Task;
import tasks.repository.ArrayTaskList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MockitoRepoTest {
    @Mock
    private Task task;

    @InjectMocks
    private ArrayTaskList repo;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        repo.add(task);
    }

    @Test
    public void test_valid_add() {
        repo.add(task);
        assertEquals(repo.size(), 2);
    }

    @Test
    public void test_valid_remove() {
        assertEquals(repo.size(), 1);
        repo.remove(task);
        assertEquals(repo.size(), 0);
    }
}
